import { Grid, Paper } from "@mui/material"

function Introduct() {
    return (
        <Grid>
            <Grid item mt={2} textAlign={"center"} color={"#ff5722"}>
                <h3><b>Tại sao lại Pizza 365:</b></h3>
                <hr style={{width:"500px",marginLeft:"300px", marginRight:"300px"}}></hr>
            </Grid>
            <Grid item xs={16}>
                <Grid container justifyContent={"center"} >
                    <Paper sx={{ height: 200, width: 280 }} style={{ textAlign: "center", backgroundColor:"yellow"}}>
                        <h3 style={{ marginTop: "20px" }}>Giới thiệu</h3>
                        <p style={{ marginTop: "30px",margin:"20px" }}>Là quán phục vụ Pizza chuẩn thương hiệu Italian</p>
                    </Paper>
                    <Paper sx={{ height: 200, width: 280 }} style={{  textAlign: "center",backgroundColor:"lightsalmon" }}>
                        <h3 style={{ marginTop: "20px" }}>Độc quyền</h3>
                        <p style={{ marginTop: "30px",margin:"20px" }}>Là nhà hàng  duy nhất tại Hà Nội được nhượng quyền từ Italian.</p>
                    </Paper>
                    <Paper sx={{ height: 200, width: 280 }} style={{  textAlign: "center",backgroundColor:"orange" }}>
                        <h3 style={{ marginTop: "20px" }}>Phụ vụ</h3>
                        <p style={{ marginTop: "30px", margin:"20px" }}>Phục vụ các món Pizza với các loại hương vị khác nhau mang tới trải nghiệm thú vị</p>
                    </Paper>
                    <Paper sx={{ height: 200, width: 280 }} style={{  textAlign: "center",backgroundColor:"#75be14" }}>
                        <h3 style={{ marginTop: "20px" }}>Chất lượng</h3>
                        <p style={{ marginTop: "30px" ,margin:"20px"}}>Mọi nguyên liêu được nhập khẩu chính hãng mang tới trải nghiệm người dùng với hương vị tối ưu nhất.</p>
                    </Paper>
                </Grid>
            </Grid>
        </Grid>
    )
}
export default Introduct