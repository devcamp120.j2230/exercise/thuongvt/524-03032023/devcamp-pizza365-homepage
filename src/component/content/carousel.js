import { Grid } from "@mui/material";
import Carousel from 'react-bootstrap/Carousel';
import img1 from "../../assetment/image/1.jpg"
import img2 from "../../assetment/image/2.jpg"
import img3 from "../../assetment/image/3.jpg"

function CarouselPizza(){
    return (
        <Grid>
            <h3><b>Pizza 365</b></h3>
            <h5 style={{ fontStyle: "italic" }}>Truly italian !</h5>
            <Grid>
                <Carousel slide={false}>
                    <Carousel.Item>
                        <img
                            className="d-block w-100"
                            src={img1}
                            alt="First slide"
                        />
                        <Carousel.Caption>
                            <h3>Pizza hawai</h3>
                            <p>Vị ngon tới từ hawai</p>
                        </Carousel.Caption>
                    </Carousel.Item>
                    <Carousel.Item>
                        <img
                            className="d-block w-100"
                            src={img2}
                            alt="Second slide"
                        />

                        <Carousel.Caption>
                            <h3>Pizza hải sản</h3>
                            <p>Vị ngon tới từ biển.</p>
                        </Carousel.Caption>
                    </Carousel.Item>
                    <Carousel.Item>
                        <img
                            className="d-block w-100"
                            src={img3}
                            alt="Third slide"
                        />

                        <Carousel.Caption>
                            <h3>Pizza nguyên bản</h3>
                            <p>Vị ngon tới italian</p>
                        </Carousel.Caption>
                    </Carousel.Item>
                </Carousel>
            </Grid>
        </Grid>
    )
}

export default CarouselPizza