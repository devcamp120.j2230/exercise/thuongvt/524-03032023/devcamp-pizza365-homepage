import { Grid } from "@mui/material"
import { Button, ListGroup } from "react-bootstrap";
import Card from 'react-bootstrap/Card';

function Pizaasize() {
    return (
        <Grid>
            <Grid item mt={2} textAlign={"center"} color={"#ff5722"}>
                <h3><b>Menu Combo Pizza 365</b></h3>
                <hr style={{ width: "500px", marginLeft: "300px", marginRight: "300px" }}></hr>
                <p><span>“Hãy chọn cỡ pizza phù hợp với bạn!”</span></p>
            </Grid>
            <Grid item xs={16}>
                <Grid container justifyContent={"center"}>
                    <Card style={{ width: '18rem',height:"fit-content",margin:"30px"}}>
                        <ListGroup variant="flush">
                            <ListGroup.Item style={{backgroundColor:"#ff8900", textAlign:"center", color:"white"}}><h3>S(Small size)</h3></ListGroup.Item>
                            <ListGroup.Item style={{textAlign:"center"}}>
                                <dl>
                                    <dt><p>Đường kính<b> 20 cm</b></p></dt>
                                    <hr></hr>
                                    <dt><p>Sườn nướng <b>2</b></p></dt>
                                    <hr></hr>
                                    <dt><p>Salad <b>200 gr</b></p></dt>
                                    <hr></hr>
                                    <dt><p>Nước ngọt <b>2</b></p></dt>
                                    <hr></hr>
                                    <dt><h2>VND <b>150.000</b></h2></dt>
                                </dl>
                            </ListGroup.Item>
                            <ListGroup.Item style={{textAlign:"center"}} >
                                <Button className="form-control" style={{backgroundColor:"#ff8900",width:"200px",color:"black"}}>Chọn</Button>
                            </ListGroup.Item>
                        </ListGroup>
                    </Card>
                    <Card style={{ width: '18rem',height:"fit-content",margin:"30px"}}>
                        <ListGroup variant="flush">
                            <ListGroup.Item style={{backgroundColor:"#ff8900", textAlign:"center", color:"white"}}><h3>M (Medium size)</h3></ListGroup.Item>
                            <ListGroup.Item style={{textAlign:"center"}}>
                                <dl>
                                    <dt><p>Đường kính<b> 25 cm</b></p></dt>
                                    <hr></hr>
                                    <dt><p>Sườn nướng <b>4</b></p></dt>
                                    <hr></hr>
                                    <dt><p>Salad <b>300 gr</b></p></dt>
                                    <hr></hr>
                                    <dt><p>Nước ngọt <b>3</b></p></dt>
                                    <hr></hr>
                                    <dt><h2>VND <b>200.000</b></h2></dt>
                                </dl>
                            </ListGroup.Item>
                            <ListGroup.Item style={{textAlign:"center"}} >
                                <Button className="form-control" style={{backgroundColor:"#ff8900",width:"200px",color:"black"}}>Chọn</Button>
                            </ListGroup.Item>
                        </ListGroup>
                    </Card>
                    <Card style={{ width: '18rem',height:"fit-content",margin:"30px" }}>
                        <ListGroup variant="flush">
                            <ListGroup.Item style={{backgroundColor:"#ff8900", textAlign:"center", color:"white"}}><h3>L (Large size )</h3></ListGroup.Item>
                            <ListGroup.Item style={{textAlign:"center"}}>
                                <dl>
                                    <dt><p>Đường kính<b> 30 cm</b></p></dt>
                                    <hr></hr>
                                    <dt><p>Sườn nướng <b>8</b></p></dt>
                                    <hr></hr>
                                    <dt><p>Salad <b>500 gr</b></p></dt>
                                    <hr></hr>
                                    <dt><p>Nước ngọt <b>4</b></p></dt>
                                    <hr></hr>
                                    <dt><h2>VND <b>250.000</b></h2></dt>
                                </dl>
                            </ListGroup.Item>
                            <ListGroup.Item style={{textAlign:"center"}} >
                                <Button className="form-control" style={{backgroundColor:"#ff8900",width:"200px", color:"black"}}>Chọn</Button>
                            </ListGroup.Item>
                        </ListGroup>
                    </Card>
                </Grid>

            </Grid>
        </Grid>
    )
}
export default Pizaasize