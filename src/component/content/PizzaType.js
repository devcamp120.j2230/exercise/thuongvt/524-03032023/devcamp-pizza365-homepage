import { Grid } from "@mui/material"
import Card from 'react-bootstrap/Card';
import { Button} from "react-bootstrap";
import img1 from "../../assetment/image/hawaiian.jpg"
import img2 from "../../assetment/image/2.jpg"
import img3 from "../../assetment/image/bacon.jpg"

function PizzaType() {
    return (
        <Grid>
            <Grid item mt={2} textAlign={"center"} color={"#ff5722"}>
                <h3><b>Chọn loại pizza</b></h3>
                <hr style={{ width: "500px", marginLeft: "300px", marginRight: "300px" }}></hr>
            </Grid>
            <Grid item xs={16}>
                <Grid container justifyContent={"center"}>
                <Card style={{ width: '18rem', height: "fit-content", margin: "30px" }}>
                    <Card style={{ width:'18rem'}}>
                        <Card.Img variant="top" src={img1} />
                        <Card.Body >
                            <h6 class="card-title">OCEAN MANINA</h6>
                          <p><samp>PIZZA HẢI SẢN SỐT VỚI MAYONNAISEA</samp></p>
                          <p class="card-text">Pizza mang hương vị của Hải sản.</p>
                        </Card.Body>
                        <Card.Footer style={{textAlign:"center"}}>
                            <Button className="form-control" style={{width:"200px"}}>Chọn</Button>
                        </Card.Footer>
                    </Card>
                    </Card>
                    <Card style={{ width: '18rem', height: "fit-content", margin: "30px" }}>
                    <Card style={{ width:'18rem'}}>
                        <Card.Img variant="top" src={img2} />
                        <Card.Body >
                        <h6 class="card-title">HAIWAIIN</h6>
                          <p><samp>PIZZA DĂM BÔNG DỨA KIỂU HAWAI</samp></p>
                          <p class="card-text">Pizaa mang hương vị biển Hawai</p>
                        </Card.Body>
                        <Card.Footer style={{textAlign:"center"}}>
                            <Button className="form-control" style={{width:"200px"}}>Chọn</Button>
                        </Card.Footer>
                    </Card>
                    </Card>
                    <Card style={{ width: '18rem', height: "fit-content", margin: "30px" }}>
                    <Card style={{ width:'18rem'}}>
                        <Card.Img variant="top" src={img3} />
                        <Card.Body >
                          <h6 class="card-title">CHEESY CHICKEN BACON </h6>
                          <p><samp>PIZZA GÀ PHO MAI THỊT HEO XÔNG KHÓI</samp></p>
                          <p class="card-text">Loại Pizza truyền thống Italian.</p>
                        </Card.Body>
                        <Card.Footer style={{textAlign:"center"}}>
                            <Button className="form-control" style={{width:"200px"}}>Chọn</Button>
                        </Card.Footer>
                    </Card>
                    </Card>
                </Grid>
            </Grid>
        </Grid>
    )
}
export default PizzaType