import { Grid } from "@mui/material"
import axios from "axios";
import { useEffect, useState } from "react"
import { Form } from "react-bootstrap"

function DrinkSelect() {
        const [drinks, setDrinks]= useState([]);

        const axiosCall = async (url, body) => {
            const response = await axios(url, body);
            console.log(response.data)
            return response.data;
        };

        const getAllDrink = () => {
            axiosCall("http://203.171.20.210:8080/devcamp-pizza365/drinks")
                .then(result =>setDrinks(result))
                .catch(error => console.log('error', error.message));
        };

        useEffect(() => {
            getAllDrink()
        }, []);

    return (
        <Grid>
            <Grid item mt={2} textAlign={"center"} color={"#ff5722"}>
                <h3><b>Chọn đồ uống</b></h3>
                <hr style={{ width: "500px", marginLeft: "300px", marginRight: "300px" }}></hr>
            </Grid>
            <Grid>
                <Form.Select aria-label="Default select example">
                    <option>Chọn đồ uống</option>
                    {drinks.map((element,index)=>{
                        return <option key={index} value={element.maNuocUong}>{element.tenNuocUong}</option>
                    })}
                    
                </Form.Select>
            </Grid>
        </Grid>
    )
}
export default DrinkSelect