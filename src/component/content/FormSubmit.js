import { Grid } from "@mui/material"
import { Button, Form } from "react-bootstrap"

function FromSubit() {
    return (
        <Grid>
            <Grid item mt={2} textAlign={"center"} color={"#ff5722"}>
                <h3><b>Gửi đơn hàng</b></h3>
                <hr style={{ width: "500px", marginLeft: "300px", marginRight: "300px" }}></hr>
            </Grid>
            <Grid container>
                <Form.Label >Tên</Form.Label>
                <Form.Control type="text" id="inpTen" placeholder="Hãy nhập tên của bạn"/>
                <Form.Label >Emai</Form.Label>
                <Form.Control type="text" id="inpEmail" placeholder="Hãy nhập email của bạn"/>
                <Form.Label >Số điện thoại</Form.Label>
                <Form.Control type="text" id="inpDienThoai" placeholder="Hãy nhập số điện thoại của bạn"/>
                <Form.Label >Địa chỉ</Form.Label>
                <Form.Control type="text" id="inpDiaChi" placeholder="Hãy nhập địa chỉ của bạn"/>
                <Form.Label >Mã Giảm giá</Form.Label>
                <Form.Control type="text" id="inpDiaChi" placeholder="Nhập mã giảm giá nếu có"/>
                <Form.Label >Lời nhắn</Form.Label>
                <Form.Control type="text" id="inpDiaChi" placeholder="Nhập lời nhắn tới cửa hàng"/>
                <Button className="form-control" style={{marginTop:"10px", backgroundColor:"#ff9800"}}>Gửi</Button>
            </Grid>
        </Grid>
    )
}
export default FromSubit