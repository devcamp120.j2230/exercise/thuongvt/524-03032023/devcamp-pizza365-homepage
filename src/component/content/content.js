import { Container } from "@mui/material"
import DrinkSelect from "./DrinkSelect"
import FromSubit from "./FormSubmit"
import Lading from "./Lading"
import Pizaasize from "./PizzaSize"
import PizzaType from "./PizzaType"

function Content (){
    return(
        <Container>
            <Lading></Lading>
            <Pizaasize></Pizaasize>
            <PizzaType></PizzaType>
            <DrinkSelect></DrinkSelect>
            <FromSubit></FromSubit>
        </Container>
    )
}
export default Content