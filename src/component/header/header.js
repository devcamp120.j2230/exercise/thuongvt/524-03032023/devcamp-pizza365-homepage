import { Grid } from "@mui/material"

function Header(){
    return(
        <Grid display="flex" justifyContent={"space-around"} bgcolor="orange" >
            <Grid item xs={3} mt={2} mb={2}>
                <a href="#">Trang chủ</a>
            </Grid>
            <Grid item xs={3} mt={2} mb={2}>
                <a href="#">Combo</a>
            </Grid>
            <Grid item xs={3} mt={2} mb={2}>
                <a href="#">Loại Pizza</a>
            </Grid>
            <Grid item xs={3} mt={2} mb={2}>
                <a href="#">Giử đơn hàng</a>
            </Grid>
        </Grid>
    )
}

export default Header