import { Grid } from "@mui/material"
import UploadIcon from '@mui/icons-material/Upload';
import FacebookIcon from '@mui/icons-material/Facebook';
import InstagramIcon from '@mui/icons-material/Instagram';
import WhatsAppIcon from '@mui/icons-material/WhatsApp';
import PinterestIcon from '@mui/icons-material/Pinterest';
import TwitterIcon from '@mui/icons-material/Twitter';
import LinkedInIcon from '@mui/icons-material/LinkedIn';

function Footer (){
    return(
        <Grid textAlign={"center"} bgcolor="orange" mt={2}>
            <Grid item paddingTop={"40px"}>
            <h5>Footer</h5>
            </Grid>
            <Grid mt={1}>
            <a href="#" ><UploadIcon></UploadIcon>To the top</a>
            </Grid>
            <Grid  mt={1}>
                <a href="#"><FacebookIcon></FacebookIcon></a>
                <a href="#"><InstagramIcon></InstagramIcon></a>
                <a href="#"><WhatsAppIcon></WhatsAppIcon></a>
                <a href="#"><PinterestIcon></PinterestIcon></a>
                <a href="#"><TwitterIcon></TwitterIcon></a>
                <a href="#"><LinkedInIcon></LinkedInIcon></a>
            </Grid>
            <Grid paddingBottom={"30px"} mt={1}>
            <p>Powered by DEVCAMP</p>
            </Grid>
        </Grid>
    )
}

export default Footer